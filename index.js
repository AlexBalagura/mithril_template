var express = require('express');
var middleware = require('./middleware');
var m = require('mithril');
var render = require('mithril-node-render');
var component = require('./app/main');

var app = express();

app.use(middleware.first)
app.use('/static', express.static('static'));

app.get('/', function(req, res){
  console.log(component);
  res.send(render(m('html',[
    m('title','Main Page'),
    m('body',[
      m('div',{id: 'app'},[
        m.component(component),
      ]),
      m('div', 'finish!'),
      m('script', {'src': '/static/js/main_client.js'})
    ])
  ])));
})

app.get('/users/:id', function(req, res){
  res.send('Another page for userid ' + req.params.id);
})

var server = app.listen(3000, function(){
  var host = server.address().address;
  var port = server.address().port;
  console.log('Example app listening at http://localhost:%s', port);
})
