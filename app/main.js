var m = require('mithril');

var main = function(){
  var self = this;
  self.name = m.prop();

  self.view = function(controller, attrs){
    return m('div', [
      m('input', {type: 'text', placeholder: 'Input your name', onkeyup: m.withAttr('value', self.name)}),
      m('div', self.name())
    ]);
  }

  self.controller = function(attrs){}
}

module.exports = new main();
