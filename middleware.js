var first = function(req, res, next){
  var time = new Date();
  req.curTime = time;
  console.log('Current time %s', time);
  next()
}

exports.first = first;
